#!/bin/sh -e

# prep for build
rm -rf ./site
mkdir -p ./site
mkdir -p ./site/links
mkdir -p ./site/blog
mkdir -p ./site/now

# build top-level pages
cat head.html home.html foot.html > ./site/index.html
cat head.html links.html foot.html > ./site/links/index.html
cat head.html blog.html foot.html > ./site/blog/index.html
cat head.html now.html foot.html > ./site/now/index.html


# copy one-offs
cp style.css ./site/style.css
cp robots.txt ./site/robots.txt
cp ./*.jpg favicon.ico ./site/.

# build feed
cp ./feed/feed.atom ./site/feed.atom

# build posts
for dir in ~/code/stillgreenmossdotnet/posts/*/
do
    fulldir=${dir%*/}
    shortdir=$(basename "$fulldir")

    mkdir -p ./site/blog/"$shortdir"
    cp "$fulldir"/index.html ./site/blog/"$shortdir"/index.incomplete
    cat head.html ./site/blog/"$shortdir"/index.incomplete foot.html > ./site/blog/"$shortdir"/index.html
    rm ./site/blog/"$shortdir"/index.incomplete
done
