#!/bin/sh -e

ssh root@stillgreenmoss.net "mkdir -p /srv/stillgreenmoss"
rsync -avzP --delete ./site/. root@stillgreenmoss.net:/srv/stillgreenmoss/.
ssh root@stillgreenmoss.net "chown -R root:root /srv/stillgreenmoss"
ssh root@stillgreenmoss.net "chmod -R 755 /srv/stillgreenmoss"
