#!/bin/sh -e

mkdir -p ./posts

# get name and url and other values we need
printf "Title of blog post: "
read -r postTitle
printf "URL string for blog post: "
read -r postUrl
fullUrl='https:\/\/stillgreenmoss\.net\/blog\/'"$postUrl"
currentMonth=$(date +%B)
currentYear=$(date +%Y)
postDate=$(date --rfc-3339=date)
rssDate="$postDate"T00:00:00Z

# create link to post on blog.html
sed -i "s/<ul><!--boop-->/<ul><!--boop-->\n<li><a href=\"\/blog\/$postUrl\">$postTitle | $currentMonth $currentYear<\/a><\/li>/" blog.html

# create folder for the post in ./posts
mkdir -p ./posts/"$postUrl"

# prompt user for html for the post
"${EDITOR:-vi}" ./posts/"$postUrl"/post.html

# put post in the place it lives
mv ./posts/"$postUrl"/post.html ./posts/"$postUrl"/index.html

### update atom feed

# make new entry
cp ./feed/entry-template.atom ./feed/new-entry.atom
sed -i "s/TITLE/$postTitle/" ./feed/new-entry.atom
sed -i "s/URL/$fullUrl/g" ./feed/new-entry.atom
sed -i "s/UPDATED/$rssDate/" ./feed/new-entry.atom

# add entry to body.atom
cat ./feed/new-entry.atom ./feed/body.atom > ./feed/new-body.atom
mv ./feed/new-body.atom ./feed/body.atom
rm ./feed/new-entry.atom

# update date in head.atom
sed "s/UPDATED/$rssDate/" ./feed/head-template.atom > ./feed/head.atom

# make the whole feed
cat ./feed/head.atom ./feed/body.atom ./feed/foot.atom > ./feed/feed.atom
